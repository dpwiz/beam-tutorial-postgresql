{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE UndecidableInstances #-} -- HasSqlValueSyntax

module Main where

import Universum hiding (Product)

import Control.Lens (makeLenses)
import Control.Monad (return)
import Database.Beam
import qualified Database.Beam.Postgres as PG
import Database.Beam.Backend.SQL (HasSqlValueSyntax(..), autoSqlValueSyntax)
import Database.Beam.Backend.SQL.BeamExtensions (runInsertReturningList)
import qualified Database.PostgreSQL.Simple as PGS
import qualified Database.PostgreSQL.Simple.FromField as PGS
import Data.Time (LocalTime)
import Data.String.QQ (s)
import qualified System.Console.ANSI as ANSI

-- * Tables

-- ** User

data UserT f = User
  { _userEmail     :: Columnar f Text
  , _userFirstName :: Columnar f Text
  , _userLastName  :: Columnar f Text
  , _userPassword  :: Columnar f Text
  } deriving Generic

type User = UserT Identity
type UserId = PrimaryKey UserT Identity

-- BUG: https://github.com/tathougies/beam/issues/114
-- User
--   (LensFor userEmail)
--   (LensFor userFirstName)
--   (LensFor userLastName)
--   (LensFor userPassword)
--   = tableLenses
makeLenses ''UserT

deriving instance Eq User
deriving instance Show User
deriving instance Show (PrimaryKey UserT Identity)

instance Beamable UserT

instance Table UserT where
  data PrimaryKey UserT f = UserId (Columnar f Text)
    deriving Generic
  primaryKey = UserId . _userEmail

instance Beamable (PrimaryKey UserT)

-- ** Address (for a user)

data AddressT f = Address
  { _addressId      :: C f (Auto Int)
  , _addressLine1   :: C f Text
  , _addressLine2   :: C f (Maybe Text)
  , _addressCity    :: C f Text
  , _addressState   :: C f Text
  , _addressZip     :: C f Text
  , _addressForUser :: PrimaryKey UserT f
  } deriving Generic

type Address = AddressT Identity
deriving instance Show Address

instance Beamable AddressT

instance Table AddressT where
  data PrimaryKey AddressT f = AddressId (Columnar f (Auto Int))
    deriving Generic
  primaryKey = AddressId . _addressId

instance Beamable (PrimaryKey AddressT)
deriving instance Show (PrimaryKey AddressT Identity)

-- BUG: https://github.com/tathougies/beam/issues/114
-- Address
--   (LensFor addressId)
--   (LensFor addressLine1)
--   (LensFor addressLine2)
--   (LensFor addressCity)
--   (LensFor addressState)
--   (LensFor addressZip)
--   (UserId (LensFor addressForUserId))
--   = tableLenses
makeLenses ''AddressT

-- ** Product

data ProductT f = Product
  { _productId          :: C f (Auto Int)
  , _productTitle       :: C f Text
  , _productDescription :: C f Text
  , _productPrice       :: C f Int -- ^ Price in cents
  } deriving Generic

type Product = ProductT Identity
deriving instance Show Product

instance Beamable ProductT

instance Table ProductT where
  data PrimaryKey ProductT f = ProductId (Columnar f (Auto Int))
    deriving Generic
  primaryKey = ProductId . _productId

instance Beamable (PrimaryKey ProductT)
deriving instance Show (PrimaryKey ProductT Identity)

makeLenses ''ProductT

-- ** Shipping info (for an order)

data ShippingCarrier
  = USPS
  | FedEx
  | UPS
  | DHL
  deriving (Show, Read, Eq, Ord, Enum)

instance HasSqlValueSyntax be String => HasSqlValueSyntax be ShippingCarrier where
  sqlValueSyntax = autoSqlValueSyntax

instance PGS.FromField ShippingCarrier where
  fromField f mdata = case fmap (readEither @Text . decodeUtf8) mdata of
    Nothing ->
      PGS.returnError PGS.UnexpectedNull f ""
    Just (Left err) ->
      PGS.returnError PGS.ConversionFailed f (toString err)
    Just (Right val) ->
      pure val

instance FromBackendRow PG.Postgres ShippingCarrier

data ShippingInfoT f = ShippingInfo
  { _shippingInfoId             :: C f (Auto Int)
  , _shippingInfoCarrier        :: C f ShippingCarrier
  , _shippingInfoTrackingNumber :: C f Text
  } deriving Generic

type ShippingInfo = ShippingInfoT Identity
deriving instance Show ShippingInfo

instance Beamable ShippingInfoT

instance Table ShippingInfoT where
  data PrimaryKey ShippingInfoT f = ShippingInfoId (Columnar f (Auto Int))
    deriving Generic
  primaryKey = ShippingInfoId . _shippingInfoId

instance Beamable (PrimaryKey ShippingInfoT)
deriving instance Show (PrimaryKey ShippingInfoT (Nullable Identity))

makeLenses ''ShippingInfoT

-- ** Order

data OrderT f = Order
  { _orderId            :: Columnar f (Auto Int)
  , _orderDate          :: Columnar f LocalTime
  , _orderForUser       :: PrimaryKey UserT f
  , _orderShipToAddress :: PrimaryKey AddressT f
  , _orderShippingInfo  :: PrimaryKey ShippingInfoT (Nullable f)
  } deriving Generic

type Order = OrderT Identity
deriving instance Show Order

instance Beamable OrderT

instance Table OrderT where
  data PrimaryKey OrderT f = OrderId (Columnar f (Auto Int))
    deriving Generic
  primaryKey = OrderId . _orderId

instance Beamable (PrimaryKey OrderT)
deriving instance Show (PrimaryKey OrderT Identity)

makeLenses ''OrderT

-- * Line Item (of an order, with quantity of products)

data LineItemT f = LineItem
  { _lineItemInOrder    :: PrimaryKey OrderT f
  , _lineItemForProduct :: PrimaryKey ProductT f
  , _lineItemQuantity   :: Columnar f Int
  } deriving Generic

type LineItem = LineItemT Identity
deriving instance Show LineItem

instance Beamable LineItemT

instance Table LineItemT where
  data PrimaryKey LineItemT f = LineItemId (PrimaryKey OrderT f) (PrimaryKey ProductT f)
    deriving Generic
  primaryKey = LineItemId <$> _lineItemInOrder <*> _lineItemForProduct

instance Beamable (PrimaryKey LineItemT)

makeLenses ''LineItemT

-- * Database

data ShoppingCartDb f = ShoppingCartDb
  { _shoppingCartUsers         :: f (TableEntity UserT)
  , _shoppingCartUserAddresses :: f (TableEntity AddressT)
  , _shoppingCartProducts      :: f (TableEntity ProductT)
  , _shoppingCartOrders        :: f (TableEntity OrderT)
  , _shoppingCartShippingInfos :: f (TableEntity ShippingInfoT)
  , _shoppingCartLineItems     :: f (TableEntity LineItemT)
  } deriving Generic

instance Database ShoppingCartDb

-- BUG: https://github.com/tathougies/beam/issues/114
-- ShoppingCartDb
--   (TableLens shoppingCartUsers)
--   (TableLens shoppingCartUserAddresses)
--   = dbLenses
makeLenses ''ShoppingCartDb

shoppingCartDb :: DatabaseSettings be ShoppingCartDb
shoppingCartDb = withDbModification defaultDbSettings dbModification
  { _shoppingCartUserAddresses =
      modifyTable (const "addresses") tableModification
        { _addressLine1 = fieldNamed "address1"
        , _addressLine2 = fieldNamed "address2"
        }
  , _shoppingCartProducts =
      modifyTable (const "products") tableModification
  , _shoppingCartOrders =
      modifyTable (const "orders") tableModification
        { _orderShippingInfo = ShippingInfoId "shipping_info__id"
        }
  , _shoppingCartShippingInfos =
      modifyTable (const "shipping_info") tableModification
        { _shippingInfoId = "id"
        , _shippingInfoCarrier = "carrier"
        , _shippingInfoTrackingNumber = "tracking_number"
        }
  , _shoppingCartLineItems =
      modifyTable (const "line_items") tableModification
  }

-- * Woo, code!

main :: IO ()
main = do
  conn <- PG.connectPostgreSQL ""

  -- Tutorial 1-3, schemas

  PGS.execute_ conn [s| DROP TABLE IF EXISTS cart_users |]
  PGS.execute_ conn [s|
    CREATE TABLE cart_users
    ( email      VARCHAR NOT NULL
    , first_name VARCHAR NOT NULL
    , last_name  VARCHAR NOT NULL
    , password   VARCHAR NOT NULL
    , PRIMARY KEY( email )
    )
  |]

  PGS.execute_ conn [s| DROP TABLE IF EXISTS addresses |]
  PGS.execute_ conn [s|
    CREATE TABLE addresses
    ( id              SERIAL  PRIMARY KEY
    , address1        VARCHAR NOT NULL
    , address2        VARCHAR
    , city            VARCHAR NOT NULL
    , state           VARCHAR NOT NULL
    , zip             VARCHAR NOT NULL
    , for_user__email VARCHAR NOT NULL
    )
  |]

  PGS.execute_ conn [s| DROP TABLE IF EXISTS products |]
  PGS.execute_ conn [s|
    CREATE TABLE products
    ( id          SERIAL  PRIMARY KEY
    , title       VARCHAR NOT NULL
    , description VARCHAR NOT NULL
    , price       INT     NOT NULL
    )
  |]

  PGS.execute_ conn [s| DROP TABLE IF EXISTS orders |]
  PGS.execute_ conn [s|
    CREATE TABLE orders
    ( id                  SERIAL    PRIMARY KEY
    , date                TIMESTAMP NOT NULL
    , for_user__email     VARCHAR   NOT NULL
    , ship_to_address__id INT       NOT NULL
    , shipping_info__id   INT
    )
  |]

  PGS.execute_ conn [s| DROP TABLE IF EXISTS shipping_info |]
  PGS.execute_ conn [s|
    CREATE TABLE shipping_info
    ( id              SERIAL  PRIMARY KEY
    , carrier         VARCHAR NOT NULL
    , tracking_number VARCHAR NOT NULL
    )
  |]

  PGS.execute_ conn [s| DROP TABLE IF EXISTS line_items |]
  PGS.execute_ conn [s|
    CREATE TABLE line_items
    (item_in_order__id     INTEGER NOT NULL
    , item_for_product__id INTEGER NOT NULL
    , item_quantity        INTEGER NOT NULL
    )
  |]

  withDatabaseDebug logSql conn $ do
    -- Tutorial 3, fixtures
    let
      james = User "james@example.com" "James" "Smith" "b4cc344d25a2efe540adbf2678e2304c"
      betty = User "betty@example.com" "Betty" "Jones" "82b054bd83ffad9b6cf8bdb98ce3cc2f"
      sam = User "sam@example.com" "Sam" "Taylor" "332532dcfaa1cbf61e2a266bd723612c"
      users = [james, betty, sam]

      addresses =
        [ Address (Auto Nothing) "123 Little Street" Nothing "Boston" "MA" "12345" (pk james)
        , Address (Auto Nothing) "222 Main Street" (Just "Ste 1") "Houston" "TX" "8888" (pk betty)
        , Address (Auto Nothing) "9999 Residence Ave" Nothing "Sugarland" "TX" "8989" (pk betty)
        ]

      products =
        [ Product (Auto Nothing) "Red Ball" "A bright red, very spherical ball" 1000
        , Product (Auto Nothing) "Math Textbook" "Contains a lot of important math theorems and formulae" 2500
        , Product (Auto Nothing) "Intro to Haskell" "Learn the best programming language in the world" 3000
        , Product (Auto Nothing) "Suitcase" "A hard durable suitcase" 15000
        ]

    runInsert . insert (shoppingCartDb ^. shoppingCartUsers) $ insertValues users

    [jamesAddress1, bettyAddress1, bettyAddress2] <- runInsertReturningList
      (shoppingCartDb ^. shoppingCartUserAddresses)
      (insertValues addresses)

    [redBall, mathTextbook, introToHaskell, suitcase] <- runInsertReturningList
      (shoppingCartDb ^. shoppingCartProducts)
      (insertValues products)

    [bettyShippingInfo] <- runInsertReturningList
      (shoppingCartDb ^. shoppingCartShippingInfos)
      (insertValues [ ShippingInfo (Auto Nothing) USPS "12345790ABCDEFGHI" ])

    [jamesOrder1, bettyOrder1, jamesOrder2] <- runInsertReturningList
      (shoppingCartDb ^. shoppingCartOrders)
      (insertExpressions
        [ Order
            (val_ (Auto Nothing))
            currentTimestamp_
            (val_ (pk james))
            (val_ (pk jamesAddress1))
            nothing_
        , Order
            (val_ (Auto Nothing))
            currentTimestamp_
            (val_ (pk betty))
            (val_ (pk bettyAddress1))
            (just_ (val_ (pk bettyShippingInfo)))
        , Order
            (val_ (Auto Nothing))
            currentTimestamp_
            (val_ (pk james))
            (val_ (pk jamesAddress1))
            nothing_
        ]
      )

    let
      lineItems =
        [ LineItem (pk jamesOrder1) (pk redBall) 10
        , LineItem (pk jamesOrder1) (pk mathTextbook) 1
        , LineItem (pk jamesOrder1) (pk introToHaskell) 4

        , LineItem (pk bettyOrder1) (pk mathTextbook) 3
        , LineItem (pk bettyOrder1) (pk introToHaskell) 3

        , LineItem (pk jamesOrder2) (pk mathTextbook) 1
        ]
    runInsert . insert (shoppingCartDb ^. shoppingCartLineItems) $
      insertValues lineItems

    -- Tutorial 1

    let allUsers = all_ (_shoppingCartUsers shoppingCartDb)
    runSelectReturningList (select allUsers) >>= mapM_ print

    let
      sortUsersByFirstName = orderBy_
        (\u ->
          ( asc_ (_userFirstName u)
          , desc_ (_userLastName u)
          )
        )
        allUsers
    runSelectReturningList (select sortUsersByFirstName) >>= mapM_ print

    let
      -- boundedQuery :: Q PG.PgSelectSyntax _ _ _
      boundedQuery = limit_ 1 $ offset_ 1 $ orderBy_ (asc_ . _userFirstName) allUsers
    runSelectReturningList (select boundedQuery) >>= mapM_ print

    let
      userCount = aggregate_
        (\u -> as_ @Int countAll_)
        allUsers
    runSelectReturningOne (select userCount) >>= print

    let
      numberOfUsersByName = aggregate_
        (\u ->
          ( group_ (_userFirstName u)
          , as_ @Int countAll_)
        )
        allUsers
    runSelectReturningList (select numberOfUsersByName) >>= mapM_ print

    -- Tutorial 2

    runSelectReturningList (select . all_ $ shoppingCartDb ^. shoppingCartUserAddresses) >>= mapM_ print

    uas1 <- runSelectReturningList . select $ do
      user <- all_ (shoppingCartDb ^. shoppingCartUsers)
      address <- all_ (shoppingCartDb ^. shoppingCartUserAddresses)
      guard_ (_addressForUser address `references_` user)
      pure (user, address)
    mapM_ print uas1

    uas2 <- runSelectReturningList . select $ do
      address <- all_ (shoppingCartDb ^. shoppingCartUserAddresses)
      user <- related_ (shoppingCartDb ^. shoppingCartUsers) (_addressForUser address)
      pure (user, address)
    mapM_ print uas2

    liftIO . unless (show uas1 == show uas2) $
      fail "User addresses results are not equal!"

    let bettyId = UserId "betty@example.com" :: UserId
    betty's <- runSelectReturningList . select $ do
      address <- all_ (shoppingCartDb ^. shoppingCartUserAddresses)
      guard_ (_addressForUser address ==. val_ bettyId)
      pure address
    mapM_ print betty's

    runUpdate $ save
      (shoppingCartDb ^. shoppingCartUsers)
      (james & userPassword .~ "52a516ca6df436828d9c0d26e31ef704")
    Just james' <- runSelectReturningOne $ lookup
      (shoppingCartDb ^. shoppingCartUsers)
      (UserId "james@example.com")
    putStrLn $ "James's new password is " ++ show (james' ^. userPassword)

    runUpdate $ update
      (shoppingCartDb ^. shoppingCartUserAddresses)
      (\address ->
          [ address ^. addressCity <-. val_ "Sugarville"
          , address ^. addressZip <-. val_ "12345"
          ]
      )
      (\address ->
        address ^. addressCity ==. val_ "Sugarland" &&.
        address ^. addressState ==. val_ "TX"
      )
    runSelectReturningList (select . all_ $ shoppingCartDb ^. shoppingCartUserAddresses) >>= mapM_ print

    runDelete $ delete
      (shoppingCartDb ^. shoppingCartUserAddresses)
      (\address ->
          address ^. addressCity ==. "Houston" &&.
          _addressForUser address `references_` betty
      )

    -- Tutorial 3

    usersAndOrders <- runSelectReturningList . select $ do
      user <- all_ (shoppingCartDb ^. shoppingCartUsers)
      order <- leftJoin_
        (all_ $ shoppingCartDb ^. shoppingCartOrders)
        (\order -> _orderForUser order `references_` user)
      pure (user, order)
    mapM_ print usersAndOrders

    usersWithNoOrders <- runSelectReturningList . select $ do
      user <- all_ (shoppingCartDb ^. shoppingCartUsers)
      order <- leftJoin_
        (all_ $ shoppingCartDb ^. shoppingCartOrders)
        (\order -> _orderForUser order `references_` user)
      guard_ (isNothing_ order)
      pure user
    mapM_ print usersWithNoOrders

    ordersWithCostOrdered <- runSelectReturningList
      . select
      . orderBy_ (desc_ . snd)
      . aggregate_ (\(order, lineItem, product) ->
          ( group_ order
          , sum_ (lineItem ^. lineItemQuantity * product ^. productPrice)
          )
        )
      $ do
        lineItem <- all_ (shoppingCartDb ^. shoppingCartLineItems)
        order <- related_ (shoppingCartDb ^. shoppingCartOrders) (_lineItemInOrder lineItem)
        product <- related_ (shoppingCartDb ^. shoppingCartProducts) (_lineItemForProduct lineItem)
        pure (order, lineItem, product)
    mapM_ print ordersWithCostOrdered

    allUsersAndTotals <- runSelectReturningList
      . select
      . orderBy_ (desc_ . snd)
      . aggregate_ (\(user, lineItem, product) ->
          ( group_ user
          , sum_ $
              maybe_ 0 identity (_lineItemQuantity lineItem) *
              maybe_ 0 identity (product ^. productPrice)
          )
        )
      $ do
        user <- all_ (shoppingCartDb ^. shoppingCartUsers)

        order <- leftJoin_
          (all_ $ shoppingCartDb ^. shoppingCartOrders)
          (\order -> _orderForUser order `references_` user)

        lineItem <- leftJoin_
          (all_ $ shoppingCartDb ^. shoppingCartLineItems)
          (\lineItem ->
            maybe_
              (val_ False)
              (\order ->
                _lineItemInOrder lineItem `references_` order
              )
              order
          )

        product <- leftJoin_
          (all_ $ shoppingCartDb ^. shoppingCartProducts)
          (\product ->
            maybe_
            (val_ False)
            (\lineItem ->
              _lineItemForProduct lineItem `references_` product
            )
            lineItem
          )
        pure (user, lineItem, product)

    mapM_ print allUsersAndTotals

    allUnshippedOrders <- runSelectReturningList
      . select
      . filter_ (isNothing_  . _orderShippingInfo)
      $ all_ (shoppingCartDb ^. shoppingCartOrders)
    mapM_ print allUnshippedOrders

    shippingInformationByUser <- runSelectReturningList . select $ do
      user <- all_ (shoppingCartDb ^. shoppingCartUsers)

      (userEmail, unshippedCount) <- subselect_
        . aggregate_ (\(userEmail, order) -> (group_ userEmail, countAll_))
        $ do
          user  <- all_ (shoppingCartDb ^. shoppingCartUsers)
          order <- leftJoin_
            (all_ $ shoppingCartDb ^. shoppingCartOrders)
            (\order ->
              _orderForUser order `references_` user &&.
              isNothing_ (_orderShippingInfo order)
            )
          pure (pk user, order)

      guard_ (userEmail `references_` user)

      (userEmail, shippedCount) <- subselect_
        . aggregate_ (\(userEmail, order) -> (group_ userEmail, countAll_))
        $ do
          user  <- all_ (shoppingCartDb ^. shoppingCartUsers)
          order <- leftJoin_
            (all_ $ shoppingCartDb ^. shoppingCartOrders)
            (\order ->
              _orderForUser order `references_` user &&.
              isJust_ (_orderShippingInfo order)
            )
          pure (pk user, order)

      guard_ (userEmail `references_` user)

      pure (user, unshippedCount, shippedCount)

    mapM_ print shippingInformationByUser

    pure ()

-- * Helpers

logSql :: String -> IO ()
logSql line = do
  case words (toText line) of
    "SELECT" : _ ->
      ANSI.setSGR [ANSI.SetColor ANSI.Foreground ANSI.Vivid ANSI.Green]
    "INSERT" : _ ->
      ANSI.setSGR [ANSI.SetColor ANSI.Foreground ANSI.Vivid ANSI.Blue]
    "UPDATE" : _ ->
      ANSI.setSGR [ANSI.SetColor ANSI.Foreground ANSI.Vivid ANSI.Yellow]
    "DELETE" : _ ->
      ANSI.setSGR [ANSI.SetColor ANSI.Foreground ANSI.Vivid ANSI.Red]
    _ ->
      pure ()
  putStrLn line
  ANSI.setSGR [ANSI.Reset]
